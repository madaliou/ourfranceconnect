const axios = require('axios');
const AxiosLogger = require('axios-logger');

const httpClient = axios.create();
if (process.env.NODE_ENV !== 'test') {
  httpClient.interceptors.request.use(AxiosLogger.requestLogger, AxiosLogger.errorLogger);
  httpClient.interceptors.response.use(AxiosLogger.responseLogger, AxiosLogger.errorLogger);
}

module.exports = {
  httpClient
};
