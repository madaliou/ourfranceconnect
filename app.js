const express = require('express');
const crypto = require('crypto');
const session = require('express-session');
const { requestDataInfo, requestToken, requestUserInfo } = require('./helpers/userInfoHelper');
const { containsDataScopes, containsTracksScopes, getPayloadOfIdToken } = require('./helpers/utils');



const path = require('path');

const {config} = require('./config');
const { callbackParamsValidatorMiddleware } = require('./validators/callbackParams');

require('dotenv').config();

const app = express();

app.set('view engine', 'ejs');


app.use(session({
  secret: 'your_secret_key',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false } // Pour développement, mettre à true en production avec HTTPS
}));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  if (req.session.user) {
    res.send(`<h1>Bienvenue, ${req.session.user.given_name}</h1><p>Email: ${req.session.user.email}</p>`);
  } else {
    res.sendFile(path.join(__dirname, 'public', 'login.html'));
  }
});


app.get('/login', (req, res) => {

  const claims = false;
  const eidasLevel = 'eidas1';
  const myScopes = {
    eidasLevel: 'eidas1',
    scope_openid: 'on',
    scope_given_name: 'on',
    scope_family_name: 'on',
    scope_gender: 'on',
    scope_preferred_username: 'on',
    scope_birthdate: 'on',
    claims: '{"id_token":{"amr":{"essential":true}}}',
    x: '77',
    y: '21',
  };
  const scopes = Object.keys(myScopes)
    .filter(key => key.startsWith('scope_'))
    .map(scope => scope.split('scope_').pop())
    .join(' ');

  const query = {
    scope: scopes,
    redirect_uri: `http://localhost:3000/login-callback`,
    response_type: 'code',
    client_id: '211286433e39cce01db448d80181bdfd005554b19cd51b3fe7943f6b3b86ab6e',
    state: `state${crypto.randomBytes(32).toString('hex')}`,
    nonce: `nonce${crypto.randomBytes(32).toString('hex')}`,
  };

  if (claims) {
    query.claims = claims;
  }

  // Save requested scopes in the session
  req.session.scopes = scopes;

  if (eidasLevel) {
    query.acr_values = eidasLevel;
  }

  const url = `${config.FC_URL}${config.AUTHORIZATION_FC_PATH}`;
  const params = new URLSearchParams(query).toString();
  return res.redirect(`${url}?${params}`);
});

app.get('/login-callback', callbackParamsValidatorMiddleware, async (req, res, next) => {
  try {
    const spConfig = {
      clientId: config.AUTHENTICATION_CLIENT_ID,
      clientSecret: config.AUTHENTICATION_CLIENT_SECRET,
      code: req.query.code,
      redirectUri: `${config.FS_URL}${config.LOGIN_CALLBACK_FS_PATH}`,
    };

    const { accessToken, idToken } = await requestToken(spConfig);
    if (!accessToken || !idToken) {
      return res.sendStatus(401);
    }
    const user = await requestUserInfo(accessToken);

    // Fetch the data from FD only if data scope requested
    let data = null;
    const { scopes } = req.session;
    if (containsDataScopes(scopes)) {
      data = await requestDataInfo(accessToken);
    }

    let tracks = null;
    if (containsTracksScopes(scopes)) {
      tracks = await requestTracksDataInfo(accessToken);
    }
    // Store the user and context in session so it is available for future requests
    // as the idToken for Logout
    req.session.user = user;
    req.session.data = data;
    req.session.tracks = tracks;
    req.session.idTokenPayload = getPayloadOfIdToken(idToken);
    req.session.idToken = idToken;

    return res.redirect('/user');
  } catch (error) {
    return next(error);
  }
})

app.get('/user',  (req, res) => {
  const {
    data, user, idTokenPayload = {}, tracks,
  } = req.session;
  return res.render('pages/data', {
    user,
    data,
    tracks,
    eIDASLevel: idTokenPayload.acr,
    amr: idTokenPayload.amr,
    userLink: 'https://github.com/france-connect/identity-provider-example/blob/master/database.csv',
    dataLink: 'https://github.com/france-connect/data-provider-example/blob/master/database.csv',
  });
});


app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000');
});
